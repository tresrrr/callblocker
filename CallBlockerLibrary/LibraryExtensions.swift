//
//  Extensions.swift
//  CallBlocker
//
//  Created by tresrrr on 25/05/2021.
//

import UIKit
import CallKit


public func reloadExtension(){
    CXCallDirectoryManager.sharedInstance.reloadExtension(withIdentifier: "RubenRR.CallBlocker.CallBlockerExtension", completionHandler: { (error) in
        if let error = error {
            print("Error reloading extension: \(error.localizedDescription)")
        }
    })
}

public extension StringProtocol {
    subscript(_ offset: Int)                     -> Element     { self[index(startIndex, offsetBy: offset)] }
    subscript(_ range: Range<Int>)               -> SubSequence { prefix(range.lowerBound+range.count).suffix(range.count) }
    subscript(_ range: ClosedRange<Int>)         -> SubSequence { prefix(range.lowerBound+range.count).suffix(range.count) }
    subscript(_ range: PartialRangeThrough<Int>) -> SubSequence { prefix(range.upperBound.advanced(by: 1)) }
    subscript(_ range: PartialRangeUpTo<Int>)    -> SubSequence { prefix(range.upperBound) }
    subscript(_ range: PartialRangeFrom<Int>)    -> SubSequence { suffix(Swift.max(0, count-range.lowerBound)) }
}

public extension LosslessStringConvertible {
    var string: String { .init(self) }
}

public extension BidirectionalCollection {
    subscript(safe offset: Int) -> Element? {
        guard !isEmpty, let i = index(startIndex, offsetBy: offset, limitedBy: index(before: endIndex)) else { return nil }
        return self[i]
    }
}

extension UIImage {

    func colored(_ color: UIColor) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { context in
            color.setFill()
            self.draw(at: .zero)
            context.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height), blendMode: .sourceAtop)
        }
    }

}

public extension String {
    
    subscript(idx: Int) -> String {
        String(self[index(startIndex, offsetBy: idx)])
    }
    
    var internationalPhoneNumber: String {
                
        var formattedNumber = self
        formattedNumber = formattedNumber.replacingOccurrences(of: "(", with: "")
                                         .replacingOccurrences(of: ")", with: "")
                                         .replacingOccurrences(of: " ", with: "")
                                         .replacingOccurrences(of: "_", with: "")
                                         .replacingOccurrences(of: "-", with: "")
                                         .replacingOccurrences(of: "+", with: "")
        let prefix = String(formattedNumber[0..<2])
        
        if prefix == Options.Contacts.prefix {
            return formattedNumber
        }
        
        if prefix == "00" {
            return (Options.Contacts.prefix + String(formattedNumber[3..<formattedNumber.count]))
        }
        
        return formattedNumber
    }
    
    var formatPhoneNumber: Int64 { // internationalized Int64
        return Int64(self.internationalPhoneNumber) ?? 0
    }
}
