//
//  Options.swift
//  CallBlocker
//
//  Created by tresrrr on 02/06/2021.
//

import UIKit

public enum Options {
    
    public enum General{
        public static let backgrondColor = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.00)
    }
    
    public enum Text{
        public static let firstColor = UIColor.gray
        public static let secondaryColor = UIColor.lightGray
    }
    
    public enum Button{
        public static let border = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.00)
        public static let alertBackground = UIColor(red:0.95, green:0.41, blue:0.45, alpha:1.00)
    }

    public enum TabBar{
        public static let selectedColor = UIColor(red:0.44, green:0.11, blue:0.71, alpha:1.00)
    }
    
    public enum Contacts{
        public static let titleButtonsSeparation = CGFloat(30)
        public static let prefix = "34"
    }
}
