//
//  ContactButton.swift
//  CallBlocker
//
//  Created by tresrrr on 01/06/2021.
//

import UIKit
import SnapKit

final class ContactButton: UIView {
    
    public var imageView: UIImageView?
    
    //init(image: UIImage, backColor: UIColor = Options.General.backgrondColor) {
    init(image: UIImage, backColor: UIColor = Options.General.backgrondColor) {

        super.init(frame: .zero)
                
        self.layer.cornerRadius = 20
        self.layer.borderWidth = 1
        self.layer.borderColor = Options.Button.border.cgColor
        self.backgroundColor = backColor
        
        let image = image.colored(Options.TabBar.selectedColor)
        imageView = UIImageView(image: image)
        
        guard let imageView = imageView else { return }
        imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
        
        self.snp.makeConstraints { (make) in
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        
        imageView.snp.makeConstraints { (make) in
            make.center.equalTo(self.snp.center)
            make.width.equalTo(20)
            make.height.equalTo(20)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
}
