//
//  ContactSection.swift
//  CallBlocker
//
//  Created by tresrrr on 01/06/2021.
//

import UIKit
import SnapKit

enum Social {
    case empty
    case mail
    case whatsapp
    case telegram
}

protocol ContactSectionDelegate: AnyObject {
    func sendTelegram()
    func sendWhatsapp()
    func sendMail(_ mail: String)
}

final class ContactSection: UIView {
    
    weak var presenter: ContactSectionDelegate?
    
    private var mailButton: UIImageView?
    private var whatsappButton: UIImageView?
    private var telegramButton: UIImageView?
    private var title: String?
    
    init(subTitle: String, title: String, social: [Social]) {
        super.init(frame: .zero)
        
        self.title = title
        
        self.layer.cornerRadius = 8
        self.layer.borderWidth = 1
        self.layer.borderColor = Options.Button.border.cgColor
        self.backgroundColor = Options.General.backgrondColor
        
        self.snp.makeConstraints { (make) in
            make.height.equalTo(80)
        }
     
        setup(subTitle: subTitle, title: title, social: social)
        
        let mailGesture = UITapGestureRecognizer(target: self,action: #selector(self.sendMail(_:)))
        mailButton?.addGestureRecognizer(mailGesture)
        
        let whatsappGesture = UITapGestureRecognizer(target: self,action: #selector(self.sendWhatsapp(_:)))
        whatsappButton?.addGestureRecognizer(whatsappGesture)
        
        let telegramGesture = UITapGestureRecognizer(target: self,action: #selector(self.sendTelegram(_:)))
        telegramButton?.addGestureRecognizer(telegramGesture)
    }
             
    private func setup(subTitle: String, title: String, social: [Social]) {
        
        if subTitle != "" {
            let subTitleLabel: UILabel = {
                let label = UILabel()
                label.text = subTitle
                label.textColor = Options.Text.secondaryColor
                label.font = UIFont.systemFont(ofSize: 9)
                return label
            }()
            
            self.addSubview(subTitleLabel)
            
            subTitleLabel.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(8)
                make.left.equalToSuperview().offset(8)
            }
        }
        
        let titleLabel = UILabel()
        
        if title != "" {
            titleLabel.text = title
            titleLabel.textColor = Options.Text.firstColor
            titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
            titleLabel.textAlignment = .center
            titleLabel.lineBreakMode = .byTruncatingMiddle
            self.addSubview(titleLabel)
        }
                
        if social[0] != .empty {
            let socialStack: UIStackView = {
                let stack = UIStackView()
                stack.axis = .horizontal
                stack.alignment = .center
                stack.distribution = .fillEqually
                stack.contentMode = .center
                stack.spacing = 20
                return stack
            }()
            
            self.addSubview(socialStack)
            
            socialStack.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.height.equalToSuperview().offset(-16)
                make.right.equalToSuperview().offset(-18)
            }
        
            for socialSelection in social {
                switch socialSelection {
                    case .mail:
                        mailButton = UIImageView(image: UIImage(systemName: "envelope"))
                        guard let mailButton = mailButton else {return}
                        mailButton.isUserInteractionEnabled = true
                        mailButton.contentMode = .center
                        mailButton.snp.makeConstraints { (make) in
                            make.width.height.equalTo(25)
                        }
                        socialStack.addArrangedSubview(mailButton)
                        break
                        
                    case .whatsapp:
                        if title.internationalPhoneNumber[0] == "6" || title.internationalPhoneNumber[0] == "+"{ // Se comprueba si son moviles
                            whatsappButton = UIImageView(image: UIImage(named: "whatsapp"))
                            guard let whatsappButton = whatsappButton else {return}
                            whatsappButton.isUserInteractionEnabled = true
                            whatsappButton.snp.makeConstraints { (make) in
                                make.width.height.equalTo(25)
                            }
                            socialStack.addArrangedSubview(whatsappButton)
                        }
                        break
                        
                    case .telegram:
                        if title.internationalPhoneNumber[0] == "6" || title.internationalPhoneNumber[0] == "+"{ // Se comprueba si son moviles
                            telegramButton = UIImageView(image: UIImage(named: "telegram"))
                            guard let telegramButton = telegramButton else {return}
                            telegramButton.isUserInteractionEnabled = true
                            telegramButton.snp.makeConstraints { (make) in
                                make.width.height.equalTo(25)
                            }
                            socialStack.addArrangedSubview(telegramButton)
                        }
                        break
                        
                    default: break
                }
            }
            
            
            if title != "" {
                
                let sectionStack: UIStackView = {
                    let stack = UIStackView()
                    stack.axis = .horizontal
                    stack.alignment = .center
                    stack.distribution = .fill
                    stack.contentMode = .scaleToFill
                    stack.spacing = 8
                    return stack
                }()
            
                self.addSubview(sectionStack)
                
                sectionStack.snp.makeConstraints { (make) in
                    make.centerY.equalToSuperview()
                    make.left.equalToSuperview().offset(8)
                    make.right.equalTo(socialStack.snp.left).offset(-8)
                }
            
                sectionStack.addArrangedSubview(titleLabel)
            }
        } else {
            if title != "" {
                titleLabel.snp.makeConstraints { (make) in
                    make.center.equalToSuperview()
                }
            }
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc private func sendWhatsapp(_ sender: UITapGestureRecognizer){
        presenter?.sendWhatsapp()
        print(" > ContactSection > sendWhatsapp")
    }
    
    @objc private func sendTelegram(_ sender: UITapGestureRecognizer){
        presenter?.sendTelegram()
        print(" > ContactSection > sendTelegram")
    }
    
    @objc private func sendMail(_ sender: UITapGestureRecognizer){
        guard let mail = self.title else { return }
        presenter?.sendMail(mail)
        print(" > ContactSection > sendMail")
    }
    
}

