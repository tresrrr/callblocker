//
//  Extensions.swift
//  CallBlocker
//
//  Created by tresrrr on 25/05/2021.
//

import UIKit

public extension URL {

    /// Returns a URL for the given app group and database pointing to the sqlite database.
    static func storeURL(for appGroup: String, databaseName: String) -> URL {
        guard let fileContainer = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: appGroup) else {
            fatalError("Shared file container could not be created.")
        }

        return fileContainer.appendingPathComponent("\(databaseName).sqlite")
    }
}


public extension UIFont {
    enum FontType: String {
        case regular = "-Regular"
        case bold = "-Regular_Bold"
    }
    
    static func dancingScript(style: FontType = .regular, ofSize: CGFloat = UIFont.systemFontSize) -> UIFont {
        // Opciones posibles ["DancingScript-Regular", "DancingScript-Regular_Bold"]
        return UIFont(name: "DancingScript\(style.rawValue)", size: ofSize)!
    }
}

public extension StringProtocol {
    subscript(_ offset: Int)                     -> Element     { self[index(startIndex, offsetBy: offset)] }
    subscript(_ range: Range<Int>)               -> SubSequence { prefix(range.lowerBound+range.count).suffix(range.count) }
    subscript(_ range: ClosedRange<Int>)         -> SubSequence { prefix(range.lowerBound+range.count).suffix(range.count) }
    subscript(_ range: PartialRangeThrough<Int>) -> SubSequence { prefix(range.upperBound.advanced(by: 1)) }
    subscript(_ range: PartialRangeUpTo<Int>)    -> SubSequence { prefix(range.upperBound) }
    subscript(_ range: PartialRangeFrom<Int>)    -> SubSequence { suffix(Swift.max(0, count-range.lowerBound)) }
}

public extension LosslessStringConvertible {
    var string: String { .init(self) }
}

public extension BidirectionalCollection {
    subscript(safe offset: Int) -> Element? {
        guard !isEmpty, let i = index(startIndex, offsetBy: offset, limitedBy: index(before: endIndex)) else { return nil }
        return self[i]
    }
}

public extension String {
    
    subscript(idx: Int) -> String {
        String(self[index(startIndex, offsetBy: idx)])
    }
    
    var internationalPhoneNumber: String {
                
        var formattedNumber = self
        formattedNumber = formattedNumber.replacingOccurrences(of: " ", with: "")
                                         .replacingOccurrences(of: "_", with: "")
                                         .replacingOccurrences(of: "-", with: "")
        
        if !formattedNumber.contains("+") {
            if formattedNumber[0..<2] == "00"{
                formattedNumber = formattedNumber.replacingOccurrences(of: "00", with: "+")
            } else {
                if formattedNumber[0] == "6" { formattedNumber = "+34\(formattedNumber)" }
            }
        }
        
        return formattedNumber
    }
    
    var onlyPhoneNumber: Int64 {
                
        var phoneNumber = self.internationalPhoneNumber
        
        if phoneNumber[0] == "+" {
            let start = phoneNumber.index(phoneNumber.startIndex, offsetBy: 3)
            let end = phoneNumber.index(phoneNumber.endIndex, offsetBy: 0)
            let range = start..<end
            phoneNumber = String(phoneNumber[range])
        }
        
        return Int64(phoneNumber) ?? 0
    }
    
    func callNumber() {
        guard let url = URL(string: "telprompt://\(self)"),
            UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func callFromFacetime() {
        guard let url = URL(string: "facetime://\(self)"),
            UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

public extension Dictionary {
    subscript(i: Int) -> (key: Key, value: Value) {
        return self[index(startIndex, offsetBy: i)]
    }
}

public extension UIImage {

    func colored(_ color: UIColor) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { context in
            color.setFill()
            self.draw(at: .zero)
            context.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height), blendMode: .sourceAtop)
        }
    }

}
