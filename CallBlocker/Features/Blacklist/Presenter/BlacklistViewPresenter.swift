//
//  BlacklistsViewPresenter.swift
//  CallBlocker
//
//  Created by tresrrr on 03/06/2021.
//

import UIKit
import CallBlockerData


protocol BlacklistViewPresenterDelegate {
    func reload()
    func setBadge(_ tabBarItem: Int)
    
    func popViewController()
}


final class BlacklistViewPresenter: BlacklistViewPresenterDelegate{
    
    lazy private var callBlockerData = CallBlockerData()

    var blacklistsViewControllerPresenter: BlacklistViewControllerDelegate?
    var mainPresenter: MainPresenterDelegate?
        
    func reload(){
        blacklistsViewControllerPresenter?.reload()
    }
    
    func setBadge(_ tabBarItem: Int) {
        mainPresenter?.setBadge(tabBarItem, callBlockerData.countBlocked())
    }
    
    func popViewController() {
        mainPresenter?.popViewController()
    }
}
