//
//  BlacklistViewController.swift
//  CallBlocker
//
//  Created by tresrrr on 03/06/2021.
//

import UIKit
import SnapKit
import CallBlockerData

protocol BlacklistViewControllerDelegate {
    func presentView(_ viewController: UIViewController)
    func reload()
    
    func popViewController()
}



final class BlacklistViewController: UIViewController {
    
    var presenter: BlacklistViewPresenterDelegate?
    lazy private var callBlockerData = CallBlockerData()
    private var blacklistsView: BlacklistView?
    private let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reload()
        
        //let navigationSearchBar = SearchBar()
        definesPresentationContext = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = NSLocalizedString("ContactView.contactSection.searchBar.placeholder", comment: "")
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchBar.barTintColor = .black
//        self.isTranslucent = true
        searchController.searchBar.autocapitalizationType = .none
//        self.showsCancelButton = true
        searchController.searchBar.tintColor = .black
        searchController.searchBar.searchTextField.textColor = .gray

        if let textFieldInsideSearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField,
           let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .gray
        }

    }
}


extension BlacklistViewController: BlacklistViewControllerDelegate {
    
    func reload() {
        
        if let presenter = presenter {
            blacklistsView = BlacklistView(delegate: presenter)
            blacklistsView?.frame = self.view.frame
            self.view = blacklistsView
        }
    }
    
    func presentView(_ viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func popViewController() {
        presenter?.popViewController()
    }
}


extension BlacklistViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print(" >> searchBarTextDidBeginEditing")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print(" >> searchBarTextDidEndEditing")
        
        let names = callBlockerData.getNames()
        
        var result = ""
        names.forEach { (name) in
            if name.contains(searchController.searchBar.text!) { result = name }
        }
        
        blacklistsView?.loadBlacklistedContacts(result)
        //blacklistsView?.loadBlacklistedContacts()
    }
        
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(" >> searchBar: textDidChange")
        if searchText == "" {
            blacklistsView?.loadBlacklistedContacts()
        }
    }
}
