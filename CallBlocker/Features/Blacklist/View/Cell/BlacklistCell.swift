//
//  BlacklistCell.swift
//  CallBlocker
//
//  Created by tresrrr on 24/05/2021.
//

import UIKit
import SnapKit
import CallBlockerData

protocol BlacklistViewCellDelegate {
    func isBlocked(_ phoneNumber: String) -> Bool
}

final class BlacklistCell: UITableViewCell {
    
    static let identifier = "ContactCell"
    
    lazy private var callBlockerData = CallBlockerData()
    var presenter: BlacklistsViewDelegate?
    
    var contact: Contact?
        
    private var generalStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .fill
        stack.contentMode = .scaleAspectFill
        stack.spacing = 8
        return stack
    }()
    
    private var nameTelStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillProportionally
        stack.contentMode = .scaleToFill
        stack.spacing = 0
        return stack
    }()
    
    private var contactImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.layer.cornerRadius = 25
        image.clipsToBounds = true
        #if DEBUG
        image.backgroundColor = .yellow
        #endif
        return image
    }()
    
    private var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16) // UIFont.dancingScript(style: .bold, ofSize: 20)
        label.textColor = Options.Text.firstColor
        #if DEBUG
        label.backgroundColor = .blue
        #endif
        return label
    }()
    
    private var telephoneLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15) // UIFont.dancingScript(style: .bold, ofSize: 15)
        label.textColor = Options.Text.secondaryColor
        #if DEBUG
        label.backgroundColor = .green
        #endif
        return label
    }()
    
//    private var telImage: UIImageView = {
//        let image = UIImageView(image: UIImage(named: "telephone"))
//        image.contentMode = .scaleAspectFit
//        return image
//    }()
    
    private var blockView: UIView = {
        let view = UIView(frame: .zero)
        #if DEBUG
        view.backgroundColor = .brown
        #endif
        return view
    }()
    
    private var blockButton: ContactButton?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
                
        backgroundColor = .white
        selectionStyle = .none // NO FOCUS
        
        let gesture = UITapGestureRecognizer(target: self,action: #selector(self.toBlockPressed(_:)))
        self.blockView.addGestureRecognizer(gesture)
    }
    
    @objc private func toBlockPressed(_ sender: UITapGestureRecognizer){
        
        print("ContactView: tapped on toDeletePressed")
        
        guard let phoneNumber = contact?.phoneNumbers.first else {return}
        guard let id = contact?.id else {return}
        guard let name = contact?.firstName else {return}
        
        if isBlocked(phoneNumber) {
            callBlockerData.deleteFromBlacklist(phoneNumber)
            if let blockButton = blockButton {
                blockButton.imageView?.image = UIImage(systemName: "lock.open")!.colored(Options.TabBar.selectedColor)
                blockButton.backgroundColor = Options.General.backgrondColor
            }
        } else {
            callBlockerData.addToBlacklist(id,name, phoneNumber)
            if let blockButton = blockButton {
                blockButton.imageView?.image = UIImage(systemName: "lock")!.colored(.white)
                blockButton.backgroundColor = Options.Button.alertBackground
            }
        }
        
        presenter?.setBadge(3)
        presenter?.popViewController()
    }
        
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    @objc func toCallPressed(_ sender: UITapGestureRecognizer){
        #if DEBUG
            print("toCallPressed: tapped on ContactCell")
        #endif
        
        if let contact = contact {
            //presenter?.clickTocall(contact)
            contact.phoneNumbers.first?.internationalPhoneNumber.callNumber()
        }
    }
    
    private func setupView() {
        
        contentView.addSubview(generalStack)
        contentView.addSubview(blockView)
        
        generalStack.addArrangedSubview(contactImage)
        generalStack.addArrangedSubview(nameTelStack)
        
        nameTelStack.addArrangedSubview(nameLabel)
        nameTelStack.addArrangedSubview(telephoneLabel)
        
        self.blockButton = ContactButton(image: UIImage(systemName: "lock")!)
        if let blockButton = blockButton {
            blockButton.imageView?.image = UIImage(systemName: "lock")!.colored(.white)
            blockButton.backgroundColor = Options.Button.alertBackground
            blockView.addSubview(blockButton)
        }
    }
    
    private func setupConstraints() {
                    
        generalStack.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(35)
            make.right.equalToSuperview().offset(-62)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
                        
        contactImage.snp.makeConstraints { (make) in
            make.width.equalTo(50)
            make.height.equalTo(50)
        }
                
        nameTelStack.snp.makeConstraints { (make) in
            make.height.equalToSuperview().offset(-30)
            make.centerY.equalToSuperview()
        }
                
        nameLabel.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
        }
                
        telephoneLabel.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
        }
                
        blockView.snp.makeConstraints { (make) in
            make.left.equalTo(generalStack.snp.right)
            make.right.equalToSuperview().offset(-8)
            make.height.equalToSuperview().offset(-8)
        }
        
        if let blockButton = blockButton {
            blockButton.snp.makeConstraints { (make) in
                make.centerX.equalTo(blockView.snp.centerX).offset(-8)
                make.centerY.equalTo(blockView.snp.centerY)
            }
        }
    }
    
    func setup(contact: Contact) {
        
        self.contact = contact

        setupView()
        setupConstraints()
        
        
        // Damos formato al telefono en caso de ser necesario
//        var telephone: String?
//        if let phoneNumber = contacts[indexPath.row].phoneNumbers.first?.value {
//            telephone = phoneNumber.stringValue.replacingOccurrences(of: "-", with: " ")
//        } else {
//            telephone = ""
//        }
        
        
        // Cargamos la imagen en modo seguro, si no > placeholder
        var cImage: UIImage?
        if let imageData = contact.contactImage {
            cImage = UIImage(data: imageData)
            #if DEBUG
                print("contactImage: get imageData")
            #endif
        } else {
            cImage = UIImage(named: "PlaceholderProfilePic")
            //cImage = UIImage(systemName: "person.crop.circle")
            #if DEBUG
                print("contactImage: PlaceholderProfilePic")
            #endif
        }
        
        nameLabel.text = "\(contact.firstName) \(contact.lastName)"
        telephoneLabel.text = contact.phoneNumbers.first
        contactImage.image = cImage
    }
}

extension BlacklistCell: BlacklistViewCellDelegate{
    
    func isBlocked(_ phoneNumber: String) -> Bool{
        
        if callBlockerData.isBlocked(phoneNumber) { return true }
        else {  return false }
    }
}

