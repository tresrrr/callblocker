//
//  BlacklistsView.swift
//  CallBlocker
//
//  Created by tresrrr on 24/05/2021.
//

import UIKit
import SnapKit
import Contacts
import SwiftyContacts
import CallBlockerData

protocol BlacklistsViewDelegate {
    func setBadge(_ tabBarItem: Int)
    
    func popViewController()
}


final class BlacklistView: UIView {
    
    var presenter: BlacklistViewPresenterDelegate?
    lazy private var callBlockerData = CallBlockerData()
        
    var contacts = [[CNContact]]()
    private var sectionLetters = [String]()
        
    private var blacklistsTableView: UITableView = {
        let table = UITableView(frame: .zero, style: .plain)
        table.backgroundColor = .clear
        //table.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        table.separatorStyle = .none
        table.allowsSelection = true
        return table
    }()
        
    init(delegate: BlacklistViewPresenterDelegate) {
        super.init(frame: .zero)
        
        self.backgroundColor = .white
        
        self.presenter = delegate
        
        requestAccess { [self] (responce) in
            if responce {
                print(NSLocalizedString("ContactsView.loadContacts.granted", comment: ""))
                loadBlacklistedContacts()
            } else {
                print("Error: " + NSLocalizedString("ContactsView.loadContacts.failedAccessDenied", comment: ""))
            }
        }
                
        setupView()
        setupConstraints()
        reloadData()
        reloadExtension()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadData(){
        blacklistsTableView.reloadData()
    }
    
    func loadBlacklistedContacts(_ contact: String = ""){
        
        if contact == "" {
            let ids = callBlockerData.getIDs()
            
            getContactFromID(Identifires: ids, completionHandler: { (result) in
                switch result{

                    case .success(let contacts):

                        print(NSLocalizedString("ContactsView.loadContacts.loading", comment: ""))
                        self.contacts = self.sortedContacts(contacts)
                        
                        break

                    case .failure(let error):

                        print("Error: " + NSLocalizedString("ContactsView.loadContacts.failedEnumerate", comment: ""), error)
                        break
                }
            })
        } else {
            
            //            NO vale
            //            --------------------------------------------------
            //            Esto busca en los contactos del movil no en CoreData
            //            Necesito una funcion para la busqueda y respuesta de entre los contactos de la Coredata.
            //            Pero el problema es que en el CoreData solo tenemos ID, Name y Phonenumber
            //            ¿Busqueda cruzada por ID?
            
            searchContact(SearchString: contact) { (result) in
                switch result{
                    case .success(let contacts):
                        
                        print(NSLocalizedString("ContactsView.loadContacts.loading", comment: ""))
                        self.contacts = self.sortedContacts(contacts)
                        self.blacklistsTableView.reloadData()
                        break
                        
                    case .failure(let error):
                        
                        print("Error: " + NSLocalizedString("ContactsView.loadContacts.failedEnumerate", comment: ""), error)
                        break
                }
            }
        }
                
        #if DEBUG
            print("Contacts: \(contacts.count)")
        #endif
    }
    
    private func setupView(){
        
        addSubview(blacklistsTableView)
        
        blacklistsTableView.delegate = self
        blacklistsTableView.dataSource = self
        blacklistsTableView.register(BlacklistCell.self, forCellReuseIdentifier: BlacklistCell.identifier)
        blacklistsTableView.frame = self.frame
    }
    
    private func setupConstraints() {
        
        blacklistsTableView.snp.makeConstraints { (make) in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
        }
    }
    
    //MARK: UITable sections funcs
    
    public func sectionLetter(_ contactList: [CNContact]) -> [String] {

        var newStringList = [String]()
        contactList.forEach { (contact) in
            if contact.givenName != "" {
                if !newStringList.contains(contact.givenName.uppercased()[0]) {
                    newStringList.append(contact.givenName.uppercased()[0])
                }
            } else if contact.familyName != "" {
                if !newStringList.contains(contact.familyName.uppercased()[0]) {
                    newStringList.append(contact.familyName.uppercased()[0])
                }
            } else {
                newStringList.append("*")
            }
        }
        
        return newStringList.sorted(by: { $0 < $1 })
    }
    
    
    public func sortedContacts(_ contacts: [CNContact]) -> [[CNContact]]{
        
        self.sectionLetters = sectionLetter(contacts)
        
        var contactsWithIndex  = [[CNContact]]()
        var contactsbyIndex = [CNContact]()
        
        sectionLetters.forEach { (letter) in
            contacts.forEach { (contact) in
                if contact.givenName != "" {
                    if contact.givenName.uppercased()[0] == letter {
                        contactsbyIndex.append(contact)
                    }
                } else if contact.familyName != "" {
                    if contact.familyName.uppercased()[0] == letter {
                        contactsbyIndex.append(contact)
                    }
                } else {
                    contactsbyIndex.append(contact)
                }
            }
            contactsWithIndex.append(contactsbyIndex)
            contactsbyIndex.removeAll()
        }
        
        return contactsWithIndex
    }
}


extension BlacklistView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("Pulsado el contacto: \(contacts[indexPath.section][indexPath.row].givenName)")
//
//        let contact = Contact(id: contacts[indexPath.section][indexPath.row].identifier,
//                              firstName: contacts[indexPath.section][indexPath.row].givenName,
//                              lastName: contacts[indexPath.section][indexPath.row].familyName,
//                              phoneNumbers: contacts[indexPath.section][indexPath.row].phoneNumbers.map{ $0.value.stringValue },
//                              emailAddresses: contacts[indexPath.section][indexPath.row].emailAddresses.map{ $0.value as String },
//                              organizationName: contacts[indexPath.section][indexPath.row].organizationName,
//                              contactImage: contacts[indexPath.section][indexPath.row].thumbnailImageData
//        )
//
//        presenter?.loadContact(contact)
//    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if contacts.count < 0 {
            print(1)
            return BlacklistCell()
        }

        let contact = Contact(id: contacts[indexPath.section][indexPath.row].identifier,
                              firstName: contacts[indexPath.section][indexPath.row].givenName,
                              lastName: contacts[indexPath.section][indexPath.row].familyName,
                              phoneNumbers: contacts[indexPath.section][indexPath.row].phoneNumbers.map{ $0.value.stringValue },
                              emailAddresses: contacts[indexPath.section][indexPath.row].emailAddresses.map{ $0.value as String },
                              organizationName: contacts[indexPath.section][indexPath.row].organizationName,
                              contactImage: contacts[indexPath.section][indexPath.row].thumbnailImageData
        )

        #if DEBUG
        print("firstName: \(contact.firstName)\nlastName: \(contact.lastName)\nphoneNumbers: \(contact.phoneNumbers)")
        #endif

        let cell = BlacklistCell()
        cell.setup(contact: contact)
        cell.presenter = self

        return cell
    }
    
    // MARK: Sections

    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionLetters[section]
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = "   \(sectionLetters[section])"
        label.layer.cornerRadius = 5
        label.textColor = .black
        label.backgroundColor = .white
        return label
    }

    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.sectionLetters
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contacts[section].count
    }
    
}

extension BlacklistView: BlacklistsViewDelegate {

    func setBadge(_ tabBarItem: Int) {
        presenter?.setBadge(tabBarItem)
    }
    
    func popViewController() {
        presenter?.popViewController()
    }
}

