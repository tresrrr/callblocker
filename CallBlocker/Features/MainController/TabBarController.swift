//
//  TabBarController.swift
//  CallBlocker
//
//  Created by tresrrr on 24/05/2021.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
                
        let favoritesView = MainViewController()
        let favoritesViewTabBarItem = UITabBarItem.init(title: NSLocalizedString("tabBarItem.favorites.text", comment: ""), image: UIImage(systemName: "star"), selectedImage: UIImage(systemName: "star"))
        favoritesView.tabBarItem = favoritesViewTabBarItem

        let historyView = MainViewController()
        let historyViewTabBarItem = UITabBarItem.init(title: NSLocalizedString("tabBarItem.history.text", comment: ""), image: UIImage(systemName: "phone"), selectedImage: UIImage(systemName: "phone"))
        historyView.tabBarItem = historyViewTabBarItem

        let contactsView = ContactsViewController()
        let presenter = ContactsViewPresenter()
        contactsView.presenter = presenter
        presenter.presenter = contactsView
        let contactsViewTabBarItem = UITabBarItem.init(title: NSLocalizedString("tabBarItem.contacts.text", comment: ""), image: UIImage(systemName: "person.2.square.stack"), selectedImage: UIImage(systemName: "person.2.square.stack"))
        contactsView.tabBarItem = contactsViewTabBarItem
        
        let blacklistView = MainViewController()
        let blacklistViewTabBarItem = UITabBarItem.init(title: NSLocalizedString("tabBarItem.blacklist.text", comment: ""), image: UIImage(systemName: "lock"), selectedImage: UIImage(systemName: "lock"))
        blacklistView.tabBarItem = blacklistViewTabBarItem
        
        let tabBarControllerArray = [favoritesView, historyView, contactsView, blacklistView]
        viewControllers = tabBarControllerArray.map{ UINavigationController.init(rootViewController: $0)}
        
        tabBar.backgroundColor = Options.General.backgrondColor
        tabBar.barTintColor = Options.General.backgrondColor
        tabBar.tintColor = Options.TabBar.selectedColor
        //tabBar.isTranslucent = true
        
        self.selectedIndex = 2
    }
}

