//
//  MainViewController.swift
//  CallBlocker
//
//  Created by tresrrr on 24/05/2021.
//

import UIKit
import Contacts

protocol MainViewPresenterDelegate {
    func presentView(_ viewController: UIViewController)
}

class MainViewController: UIViewController, UINavigationControllerDelegate {
             
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.navigationController?.delegate = self
        
//        if let tabBarController = tabBarController {
//            var tabImage = tabBarController.viewControllers?[tabBarController.selectedIndex].tabBarItem.image
//            tabImage = tabImage?.colored(Options.TabBar.selectedColor)
//        }
        
        self.view.backgroundColor = .white
        
        switch tabBarController!.selectedIndex {
        case 0: // favorites
            #if DEBUG
            self.view.backgroundColor = .systemGreen
            #endif
        case 1: // history
            #if DEBUG
            self.view.backgroundColor = .systemYellow
            #endif
        case 2: // contacts
            
            loadContacts()
            
            #if DEBUG
            self.view.backgroundColor = .systemPink
            #endif
        default: // blacklist
            #if DEBUG
            self.view.backgroundColor = .systemTeal
            #endif
        }
    }
    
    private func loadContacts(){
        
//        var contactsViewController: ContactsViewController {
//            let viewController = ContactsViewController()
//            let presenter = ContactsViewPresenter()
//            viewController.presenter = presenter
//            viewController.mainPresenter = self
//            presenter.presenter = viewController
//            return viewController
//        }
//
//        self.navigationSearchBar = SearchBar()
//        self.navigationItem.titleView = searchBar
//        self.view.addSubview(contactsViewController.view)
    }
}


extension MainViewController: MainViewPresenterDelegate {
    
    func presentView(_ viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
}
