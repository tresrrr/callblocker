//
//  ContactView.swift
//  CallBlocker
//
//  Created by tresrrr on 24/05/2021.
//

import UIKit
import SnapKit
import SwiftyContacts
import CallBlockerData
import CoreData

protocol ContactViewDelegate {
    func popViewController()
}


final class ContactView: UIView {
    
    lazy private var callBlockerData = CallBlockerData()
    
    var presenter: ContactsViewControllerDelegate?
    
    private var contact: Contact?
    var contactViewController: UIViewController?
    
    
    private var blockButton = ContactButton(image: UIImage(systemName: "lock.open")!)
    
    init(_ contact: Contact) {
        super.init(frame: .zero)
        
        self.contact = contact
        
        self.backgroundColor = .white
                  
        //blacklist = callBlockerData.loadBlocked()
        setup()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func toCallPressed(_ sender: UITapGestureRecognizer){
        print("ContactView: tapped on toCallPressed")
        contact?.phoneNumbers.first?.internationalPhoneNumber.callNumber()
    }

    @objc private func toMessagePressed(_ sender: UITapGestureRecognizer){
        print("ContactView: tapped on toMessagePressed")
        if let phoneNumber = contact?.phoneNumbers.first?.internationalPhoneNumber {
            presenter?.sendMessage(phoneNumber)
        } else { return }
    }
    
    @objc private func toVideoPressed(_ sender: UITapGestureRecognizer){
        print("ContactView: tapped on toVideoPressed")
        contact?.phoneNumbers.first?.internationalPhoneNumber.callFromFacetime()
    }

    @objc private func toBlockPressed(_ sender: UITapGestureRecognizer){
        print("ContactView: tapped on toDeletePressed")
        guard let phoneNumber = contact!.phoneNumbers.first else {return}
        if callBlockerData.isBlocked(phoneNumber) {
            
            callBlockerData.deleteFromBlacklist((contact?.phoneNumbers.first)!)
            blockButton.imageView?.image = UIImage(systemName: "lock.open")!.colored(Options.TabBar.selectedColor)
            blockButton.backgroundColor = Options.General.backgrondColor
            
        } else {
            
            callBlockerData.addToBlacklist((contact?.id)!,(contact?.firstName)!, (contact?.phoneNumbers.first)!)
            blockButton.imageView?.image = UIImage(systemName: "lock")!.colored(.white)
            blockButton.backgroundColor = Options.Button.alertBackground
        }
        
        presenter?.setBadge(3)
    }
    
    private func setup(){
        
        if let contact = contact {
            
            //MARK: Header Containing Image+Name+Company?
            let headerStack: UIStackView = {
                let stack = UIStackView()
                stack.axis = .vertical
                stack.alignment = .center
                stack.distribution = .fill
                stack.contentMode = .scaleAspectFill
                stack.spacing = 8
                return stack
            }()
            
            let contactImage: UIImageView = {
                let image = UIImageView()
                image.contentMode = .scaleAspectFit
                image.layer.cornerRadius = 40
                image.clipsToBounds = true
                #if DEBUG
                image.backgroundColor = .yellow
                #endif
                return image
            }()
            
            let nameLabel: UILabel = {
                let label = UILabel()
                label.font = UIFont.systemFont(ofSize: 20) // UIFont.dancingScript(style: .bold, ofSize: 20)
                label.textColor = Options.Text.firstColor
                label.textAlignment = .center
                #if DEBUG
                label.backgroundColor = .blue
                #endif
                return label
            }()
            
            let companyLabel: UILabel = {
                let label = UILabel()
                label.font = UIFont.systemFont(ofSize: 16) // UIFont.dancingScript(style: .bold, ofSize: 20)
                label.textColor = Options.Text.secondaryColor
                label.textAlignment = .center
                #if DEBUG
                label.backgroundColor = .blue
                #endif
                return label
            }()
            
            
            
            self.addSubview(headerStack)

            if let imageData = contact.contactImage {
                contactImage.image = UIImage(data: imageData)
            } else {
                contactImage.image = UIImage(named: "PlaceholderProfilePic")
            }
            
            nameLabel.text = "\(contact.firstName) \(contact.lastName)"
            
            headerStack.addArrangedSubview(contactImage)
            headerStack.addArrangedSubview(nameLabel)
            
            if contact.organizationName != "" {
                companyLabel.text = contact.organizationName
                headerStack.addArrangedSubview(companyLabel)
            }
                        
            // ----- Constraints -----
            headerStack.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(100)
                make.left.equalToSuperview()
                make.right.equalToSuperview()
            }
                contactImage.snp.makeConstraints { (make) in
                    make.width.equalTo(80)
                    make.height.equalTo(80)
                }
            // ----- Constraints -----
                        
            //MARK: Buttons Containing Call+Message+Video+Mail
            
            let buttonStack: UIStackView = {
                let stack = UIStackView()
                stack.axis = .horizontal
                stack.alignment = .center
                stack.distribution = .fill
                stack.contentMode = .scaleAspectFill
                stack.spacing = Options.Contacts.titleButtonsSeparation
                return stack
            }()
            
            let callButton = ContactButton(image: UIImage(systemName: "phone")!)
            let messageButton = ContactButton(image: UIImage(systemName: "message")!)
            let videoButton = ContactButton(image: UIImage(systemName: "video")!)
            guard let phoneNumber = contact.phoneNumbers.first else {return}
            if callBlockerData.isBlocked(phoneNumber) {
                blockButton.imageView?.image = UIImage(systemName: "lock")!.colored(.white)
                blockButton.backgroundColor = Options.Button.alertBackground
            }
            
            // ----- Button actions -----
            var gesture = UITapGestureRecognizer()
            
            gesture = UITapGestureRecognizer(target: self,action: #selector(self.toCallPressed(_:)))
            callButton.addGestureRecognizer(gesture)
            
            gesture = UITapGestureRecognizer(target: self,action: #selector(self.toMessagePressed(_:)))
            messageButton.addGestureRecognizer(gesture)
            
            gesture = UITapGestureRecognizer(target: self,action: #selector(self.toVideoPressed(_:)))
            videoButton.addGestureRecognizer(gesture)
            
            gesture = UITapGestureRecognizer(target: self,action: #selector(self.toBlockPressed(_:)))
            blockButton.addGestureRecognizer(gesture)
            // ----- Button actions -----
            
        
            self.addSubview(buttonStack)

            buttonStack.addArrangedSubview(callButton)
            buttonStack.addArrangedSubview(messageButton)
            buttonStack.addArrangedSubview(videoButton)
            buttonStack.addArrangedSubview(blockButton)
                        
            // ----- Constraints -----
            buttonStack.snp.makeConstraints { (make) in
                make.top.equalTo(headerStack.snp.bottom).offset(8)
                make.centerX.equalToSuperview()
            }
            // ----- Constraints -----
            
            
            //MARK: The Body Containing PhoneNumber+...
            
            let bodyStack: UIStackView = {
                let stack = UIStackView()
                stack.axis = .vertical
                stack.alignment = .fill
                stack.distribution = .fill
                stack.contentMode = .scaleToFill
                stack.spacing = 8
                return stack
            }()

            if contact.phoneNumbers.first != "" {
                let phoneNumber = ContactSection(
                    subTitle: NSLocalizedString("ContactView.contactSection.phoneNumber", comment: ""),
                    title: contact.phoneNumbers.first!,
                    social: [Social.whatsapp,Social.telegram]
                )
                phoneNumber.presenter = self
                
                bodyStack.addArrangedSubview(phoneNumber)
            }
            
            if contact.emailAddresses.count > 0 {
                let mailData = ContactSection(
                    subTitle: NSLocalizedString("ContactView.contactSection.mailData", comment: ""),
                    title: contact.emailAddresses.first!,
                    social: [Social.mail]
                )
                mailData.presenter = self

                bodyStack.addArrangedSubview(mailData)
            }
            
            self.addSubview(bodyStack)
                        
            // ----- Constraints -----
            bodyStack.snp.makeConstraints { (make) in
                make.top.equalTo(buttonStack.snp.bottom).offset(8)
                make.centerX.equalToSuperview()
                make.width.equalToSuperview().offset(-24)
            }
            // ----- Constraints -----
        }
    }
    
    // MARK: Control del Backbutton y carga de ControlView para contact
    
    func presentView(){
        
        contactViewController = UIViewController()
        guard let contactViewController = contactViewController else {return}
        contactViewController.view = self
        
        let backButton = UIBarButtonItem(title: NSLocalizedString("ContactsViewController.navBar.leftBarButtonItem",comment: ""),
                                         style: UIBarButtonItem.Style.done,
                                         target: self,
                                         action: #selector(self.toBackPressed(_:)) )
        contactViewController.navigationItem.leftBarButtonItem = backButton
        
        contactViewController.navigationItem.backBarButtonItem?.action = #selector(self.toBackPressed(_:))
        
        presenter?.presentView(contactViewController)
    }
    
    
    @objc private func toBackPressed(_ sender: UIGestureRecognizer){
        popViewController()
    }
    
}

extension ContactView: ContactViewDelegate {
    func popViewController(){
        contactViewController?.navigationController?.popViewController(animated: true)
    }
}

extension ContactView: ContactSectionDelegate {
    
    func sendTelegram() {
        if let phoneNumber = contact?.phoneNumbers.first {
            presenter?.sendTelegram(phoneNumber.internationalPhoneNumber)
        } else { return }
    }
    
    func sendWhatsapp() {
        if let phoneNumber = contact?.phoneNumbers.first {
            presenter?.sendWhatsapp(phoneNumber.internationalPhoneNumber)
        } else { return }
    }
    
    func sendMessage() {
        if let phoneNumber = contact?.phoneNumbers.first {
            presenter?.sendMessage(phoneNumber.internationalPhoneNumber)
        } else { return }
    }
    
    func sendMail(_ mail: String) {
        presenter?.sendMail(mail)
    }
}
