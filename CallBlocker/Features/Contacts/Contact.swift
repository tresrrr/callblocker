//
//  TabBarController.swift
//  Contact
//
//  Created by tresrrr on 24/05/2021.
//

import UIKit
import Contacts

class Contact {
  let name: String
  let surName: String
  let email: String
  var identifier: String?
  let profileImage: UIImage?
  var storedContact: CNMutableContact?
  var movileNumber: (CNLabeledValue<CNPhoneNumber>)?
  
  init(firstName: String, lastName: String, workEmail: String, profilePicture: UIImage?){
    self.name = firstName
    self.surName = lastName
    self.email = workEmail
    self.profileImage = profilePicture
  }
}

extension Contact: Equatable {
  static func ==(lhs: Contact, rhs: Contact) -> Bool{
    return lhs.name == rhs.name &&
      lhs.surName == rhs.surName &&
      lhs.email == rhs.email &&
      lhs.profileImage == rhs.profileImage
  }
}

extension Contact {
  var contactValue: CNContact {
    let contact = CNMutableContact()
    contact.givenName = name
    contact.familyName = surName
    contact.emailAddresses = [CNLabeledValue(label: CNLabelWork, value: email as NSString)]
    if let profilePicture = profileImage {
      let imageData = profilePicture.jpegData(compressionQuality: 1)
      contact.imageData = imageData
    }
    if let phoneNumberField = movileNumber {
      contact.phoneNumbers.append(phoneNumberField)
    }
    return contact.copy() as! CNContact
  }
  
  convenience init?(contact: CNContact) {
    guard let email = contact.emailAddresses.first else { return nil }
    let firstName = contact.givenName
    let lastName = contact.familyName
    let workEmail = email.value as String
    var profilePicture: UIImage?
    if let imageData = contact.imageData {
      profilePicture = UIImage(data: imageData)
    }
    self.init(firstName: firstName, lastName: lastName, workEmail: workEmail, profilePicture: profilePicture)
    if let contactPhone = contact.phoneNumbers.first {
        movileNumber = contactPhone
    }
  }
}
