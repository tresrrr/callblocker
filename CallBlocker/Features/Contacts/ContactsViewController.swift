//
//  ContactsViewController.swift
//  CallBlocker
//
//  Created by tresrrr on 03/06/2021.
//

import UIKit
import SnapKit
import MessageUI


protocol ContactsViewControllerDelegate {
    func showAlert(title: String, message: String, okAction: String)
    func showAlert(_ alert: UIAlertController)
    func presentView(_ viewController: UIViewController)
    
    func sendTelegram(_ phoneNumber: String)
    func sendWhatsapp(_ phoneNumber: String)
    func sendMessage(_ phoneNumber: String)
    func sendMail(_ mail: String)
    
    func loadContact(_ contact: Contact)
    
    func setBadge(_ tabBarItem: Int)
    func popViewController()
}


final class ContactsViewController: UIViewController {
    
    var presenter: ContactsViewPresenterDelegate?
    private var contactsView: ContactsView?
    private var contactView: ContactView?
    private let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let presenter = presenter {
            contactsView = ContactsView(delegate: presenter)
            contactsView?.frame = self.view.frame
            self.view = contactsView
        }
        
        
        //let navigationSearchBar = SearchBar()
        definesPresentationContext = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = NSLocalizedString("ContactView.contactSection.searchBar.placeholder", comment: "")
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchBar.barTintColor = .black
//        self.isTranslucent = true
        searchController.searchBar.autocapitalizationType = .none
//        self.showsCancelButton = true
        searchController.searchBar.tintColor = .black
        searchController.searchBar.searchTextField.textColor = .gray

        if let textFieldInsideSearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField,
           let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .gray
        }

    }
}

extension ContactsViewController: ContactsViewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    func setBadge(_ tabBarItem: Int) {
        presenter?.setBadge(tabBarItem)
    }
    
    func popViewController() {
        contactView?.popViewController()
    }

    func showAlert(_ alert: UIAlertController) {
        present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String, okAction: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okAction, style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: Carga del contact ViewController
    
    func loadContact(_ contact: Contact){
        self.contactView = ContactView(contact)
        guard let contactView = self.contactView else {return}
        contactView.presenter = self
        contactView.presentView()
    }
    
    func presentView(_ viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc private func toBackPressed(_ sender: UITapGestureRecognizer){
        print("\t > toBackPressed")
    }
    
    func sendMessage(_ phoneNumber: String) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = ["\(phoneNumber)".internationalPhoneNumber]
            controller.messageComposeDelegate = self
            
            show(controller, sender: self)
            //self.present(controller, animated: true, completion: nil)
        } else {
            showAlert(title: NSLocalizedString("ContactsViewController.sendMessageError.title", comment: ""),
                      message: NSLocalizedString("ContactsViewController.sendMessageError.message", comment: ""),
                      okAction: NSLocalizedString("ContactsViewController.sendMessageError.okAction", comment: ""))
        }
    }

    
    func sendWhatsapp(_ phoneNumber: String){
        let url = "whatsapp://send?phone=\(phoneNumber)"
        
        if let urlString = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let url = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.open(url as URL)
                } else {
                    let alert = UIAlertController(title: NSLocalizedString("ContactView.contactSection.whatsappAlert.title", comment: ""), message: NSLocalizedString("ContactView.contactSection.whatsappAlert.message", comment: ""), preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "OK", style: .default) {
                        UIAlertAction in
                            if let url = URL(string: "itms-apps://apple.com/app/id310633997") {
                                UIApplication.shared.open(url)
                        }
                    }
                    alert.addAction(alertAction)
                    showAlert(alert)
                    print("Error: " + NSLocalizedString("ContactView.contactSection.whatsappAlert.title", comment: ""))
                }
            }
        }
    }
    
    func sendTelegram(_ phoneNumber: String){
    
        let url = "tg://msg?to=\(phoneNumber)"
        
        if let urlString = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let url = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.open(url as URL)
                } else {
                    let alert = UIAlertController(title: NSLocalizedString("ContactView.contactSection.telegramAlert.title", comment: ""), message: NSLocalizedString("ContactView.contactSection.telegramAlert.message", comment: ""), preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "OK", style: .default) {
                        UIAlertAction in
                            if let url = URL(string: "itms-apps://apple.com/app/id686449807") {
                                UIApplication.shared.open(url)
                        }
                    }
                    alert.addAction(alertAction)
                    showAlert(alert)
                    print("Error: " + NSLocalizedString("ContactView.contactSection.telegramAlert.title", comment: ""))
                }
            }
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func sendMail(_ mail: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(mail)"])
            mail.setMessageBody("", isHTML: true)
            
            self.presentView(mail)
        } else {
            
            let supportEmail = mail
            
                
            try! UIApplication.shared.open(sendMail(mail: supportEmail, scheme: "googlegmail")!, completionHandler: { (success: Bool) in
                if !success {
                    try! UIApplication.shared.open( self.sendMail(mail: supportEmail, scheme: "mainbox-gmail")!, completionHandler: { (success: Bool) in
                        if !success {
                            try! UIApplication.shared.open( self.sendMail(mail: supportEmail, scheme: "ms-outlook")!, completionHandler: { (success: Bool) in
                                if !success {
                                    try! UIApplication.shared.open( self.sendMail(mail: supportEmail, scheme: "x-dispatch")!, completionHandler: { (success: Bool) in
                                        if !success {
                                            if let emailURL = URL(string: "mailto:\(supportEmail)"), UIApplication.shared.canOpenURL(emailURL) {
                                                UIApplication.shared.open(emailURL)
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    }
    
    func sendMail(mail: String, scheme: String) throws -> URL? {
        
        enum sendMailError: Error {
            case ErrorDomain
        }
        
        var components = URLComponents()
        components.scheme = scheme
        components.host = "compose"
        components.queryItems = [
            URLQueryItem(name: "to", value: mail),
            URLQueryItem(name: "subject", value: ""),
            URLQueryItem(name: "body", value: ""),
        ]
        
        return components.url
    }
}

extension ContactsViewController: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension ContactsViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print(" >> searchBarTextDidBeginEditing")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print(" >> searchBarTextDidEndEditing")
        
        contactsView?.loadContacts(searchController.searchBar.text!)
    }
        
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(" >> searchBar: textDidChange")
        if searchText == "" {
            contactsView?.loadContacts()
        }
    }
}
