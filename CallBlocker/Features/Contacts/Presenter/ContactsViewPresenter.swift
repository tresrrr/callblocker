//
//  ContactsViewPresenter.swift
//  CallBlocker
//
//  Created by tresrrr on 03/06/2021.
//

import UIKit
import CallBlockerData


protocol ContactsViewPresenterDelegate {
    func showAlert(title: String, message: String, okAction: String)
    func showAlert(_ alert: UIAlertController)
    func loadContact(_ contact: Contact)
    func setBadge(_ tabBarItem: Int)
    
    func popViewController()
}


final class ContactsViewPresenter: ContactsViewPresenterDelegate{
    
    var ContactsViewControllerPresenter: ContactsViewControllerDelegate?
    var mainPresenter: MainPresenterDelegate?
    lazy private var callBlockerData = CallBlockerData()
        
    func showAlert(title: String, message: String, okAction: String) {
        ContactsViewControllerPresenter?.showAlert(title: title, message: message, okAction: okAction)
    }
    
    func showAlert(_ alert: UIAlertController) {
        ContactsViewControllerPresenter?.showAlert(alert)
    }
    
    func loadContact(_ contact: Contact) {
        ContactsViewControllerPresenter?.loadContact(contact)
    }
    
    func setBadge(_ tabBarItem: Int) {
        mainPresenter?.setBadge(tabBarItem, callBlockerData.countBlocked())
    }
    
    func popViewController() {
        ContactsViewControllerPresenter?.popViewController()
    }
}
