//
//  TabBarController.swift
//  CallBlocker
//
//  Created by tresrrr on 24/05/2021.
//

import UIKit
import CallBlockerData

protocol TabBarControllerDelegate {
    func setBadge(_ tabBarItem: Int,_ count: Int)
}

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    lazy private var callBlockerData = CallBlockerData()
    var blacklistsViewPresenter: BlacklistViewPresenter?
    
    let mainController = MainController()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainController.tabBarPresenter = self
        
        delegate = self
        self.tabBarController?.delegate = self
                
        let favoritesView = MainController()
        let favoritesViewTabBarItem = UITabBarItem.init(title: NSLocalizedString("tabBarItem.favorites.text", comment: ""), image: UIImage(systemName: "star"), selectedImage: UIImage(systemName: "star"))
        favoritesView.tabBarItem = favoritesViewTabBarItem
        setBadgeColor(favoritesViewTabBarItem, false)

        let historyView = MainController()
        let historyViewTabBarItem = UITabBarItem.init(title: NSLocalizedString("tabBarItem.history.text", comment: ""), image: UIImage(systemName: "phone"), selectedImage: UIImage(systemName: "phone"))
        historyView.tabBarItem = historyViewTabBarItem
        setBadgeColor(historyViewTabBarItem, false)

        let contactsView = ContactsViewController()
        let contactsViewPresenter = ContactsViewPresenter()
        contactsView.presenter = contactsViewPresenter
        contactsViewPresenter.ContactsViewControllerPresenter = contactsView
        contactsViewPresenter.mainPresenter = mainController
        mainController.contactsPresenter = contactsViewPresenter
        let contactsViewTabBarItem = UITabBarItem.init(title: NSLocalizedString("tabBarItem.contacts.text", comment: ""), image: UIImage(systemName: "person.2.square.stack"), selectedImage: UIImage(systemName: "person.2.square.stack"))
        contactsView.tabBarItem = contactsViewTabBarItem
        setBadgeColor(contactsViewTabBarItem, false)
        
        let blacklistsView = BlacklistViewController()
        blacklistsViewPresenter = BlacklistViewPresenter()
        blacklistsView.presenter = blacklistsViewPresenter
        blacklistsViewPresenter?.blacklistsViewControllerPresenter = blacklistsView
        blacklistsViewPresenter?.mainPresenter = mainController
        mainController.blacklistsPresenter = blacklistsViewPresenter
        let blacklistsViewTabBarItem = UITabBarItem.init(title: NSLocalizedString("tabBarItem.blacklist.text", comment: ""), image: UIImage(systemName: "lock"), selectedImage: UIImage(systemName: "lock"))
        blacklistsView.tabBarItem = blacklistsViewTabBarItem
        setBadgeColor(blacklistsViewTabBarItem, false)
        
        
        let tabBarControllerArray = [favoritesView, historyView, contactsView, blacklistsView]
        viewControllers = tabBarControllerArray.map{ UINavigationController.init(rootViewController: $0)}
        
        tabBar.backgroundColor = Options.General.backgrondColor
        tabBar.barTintColor = Options.General.backgrondColor
        tabBar.tintColor = Options.TabBar.selectedColor
                
        self.selectedIndex = 2
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setBadge(3,callBlockerData.countBlocked())
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        
        if tabBarIndex == 3 {
            blacklistsViewPresenter?.reload()
            setBadge(3,callBlockerData.countBlocked())
        }
        if let tabBarItems = tabBarController.tabBar.items {
            
            for (index, tabBarItem) in tabBarItems.enumerated() {
                if index == tabBarIndex { setBadgeColor(tabBarItem, true) }
                else { setBadgeColor(tabBarItem, false) }
            }
        }

    }
    
    func setBadgeColor(_ tabBarItem: UITabBarItem,_ selected: Bool){
        if selected {
            tabBarItem.badgeColor = Options.TabBar.selectedColor
        } else {
            tabBarItem.badgeColor = UIColor.gray
        }
    }
}

extension TabBarController: TabBarControllerDelegate{
    
    func setBadge(_ tabBarItem: Int,_ count: Int) {
        if let tabItems = self.tabBar.items {
            tabItems[tabBarItem].badgeValue = String(count)
        }
    }
    
}

