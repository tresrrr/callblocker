//
//  MainController.swift
//  CallBlocker
//
//  Created by tresrrr on 24/05/2021.
//

import UIKit
import Contacts

protocol MainPresenterDelegate {
    func setBadge(_ tabBarItem: Int,_ count: Int)
    func popViewController()
}

class MainController: UIViewController, UINavigationControllerDelegate {
             
    var tabBarPresenter: TabBarControllerDelegate?
    
    var contactsPresenter: ContactsViewPresenterDelegate?
    var blacklistsPresenter: BlacklistViewPresenterDelegate?

}


extension MainController: MainPresenterDelegate {
    func setBadge(_ tabBarItem: Int, _ count: Int) {
        tabBarPresenter?.setBadge(tabBarItem, count)
    }
    
    func popViewController() {
        contactsPresenter?.popViewController()
    }
    
}
