//
//  CallBlockerData.swift
//  CallBlockerData
//
//  Created by tresrrr on 08/06/2021.
//

import Foundation
import CoreData
import CallKit

public final class CallBlockerData {
    
    public init(){}
    
    // MARK: - Core Data
    
    lazy var persistentContainer: NSPersistentContainer = {
        let momdName = "Blacklist"
        let groupName = "group.RubenRR.CallBlocker"
        let fileName = "Blacklist.sqlite"
        
        guard let modelURL = Bundle(for: type(of: self)).url(forResource: momdName, withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        guard let baseURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: groupName) else {
            fatalError("Error creating base URL for \(groupName)")
        }
        
        let storeUrl = baseURL.appendingPathComponent(fileName)
        
        let container = NSPersistentContainer(name: momdName, managedObjectModel: mom)
        
        let description = NSPersistentStoreDescription()
        
        description.url = storeUrl
        
        container.persistentStoreDescriptions = [description]
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        return container
    }()

    public var context: NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }

    // MARK: - Core Data Saving support
    
    public func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    public func fetchRequest() -> NSFetchRequest<Blacklisted> {
        let loadContacts: NSFetchRequest<Blacklisted> = Blacklisted.fetchRequest()
        let predicates = [NSPredicate]()

        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        loadContacts.predicate = predicate
        
        loadContacts.sortDescriptors = [NSSortDescriptor(key: "phoneNumber", ascending: true)]
        return loadContacts
    }
    
    public func countBlocked() -> Int{
        let manageContext = persistentContainer.viewContext
        let loadContacts: NSFetchRequest<Blacklisted> = Blacklisted.fetchRequest()

        do {
            let contactsData = try manageContext.fetch(loadContacts)
            let blacklist = contactsData as [NSManagedObject]
            return blacklist.count
            
        } catch let error as NSError{
            print("No se pudo cargar.\nError:\n\(error)\n\(error.userInfo)")
            return 0
        }
    }
    
    
    public func loadBlocked() -> [NSManagedObject] {

        let manageContext = persistentContainer.viewContext
        let loadContacts: NSFetchRequest<Blacklisted> = Blacklisted.fetchRequest()

        do {
            let contactsData = try manageContext.fetch(loadContacts)
            let blacklist = contactsData as [NSManagedObject]
            
            print("blacklisted count: \(countBlocked())")  // <<-------------
    
            return blacklist

        } catch let error as NSError{
            
            print("No se pudo cargar.\nError:\n\(error)\n\(error.userInfo)")
            
            return [NSManagedObject]()
        }
    }
    
    public func getIDs() -> [String]{
        
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Blacklisted")
    
        do {
            let records = try managedContext.fetch(fetchRequest)
            if let records = records as? [NSManagedObject] {
                
                var ids = [String]()
                records.forEach { (listRecord) in
                    ids.append( listRecord.value(forKey: "id") as! String )
                }
                return ids
            }
        } catch {
            print("Unable to fetch managed objects for entity.")
        }
           
        return []
    }
    
    public func getNames() -> [String]{
        
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Blacklisted")
    
        do {
            let records = try managedContext.fetch(fetchRequest)
            if let records = records as? [NSManagedObject] {
                
                var ids = [String]()
                records.forEach { (listRecord) in
                    ids.append( listRecord.value(forKey: "name") as! String )
                }
                return ids
            }
        } catch {
            print("Unable to fetch managed objects for entity.")
        }
           
        return []
    }
    
    
    
    
    
    
    public func isBlocked(_ phoneNumber: String) -> Bool{
        
        let managedContext = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Blacklisted")
        fetchRequest.predicate = NSPredicate(format: "phoneNumber == %lld", phoneNumber.formatPhoneNumber)

        do {
            let results = try managedContext.fetch(fetchRequest)

            if results.count > 0{ return true
            } else { return false }
            
        } catch {
            print("Error: " + NSLocalizedString("ContactView.contactSection.coreData.fetchError", comment: ""))
            
            return false
        }
    }
    
    
    
    
    
    
    public func addToBlacklist(_ id: String, _ name: String,_ phoneNumber: String) {
        
        let managedContext = persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Blacklisted", in: managedContext)!
        let blacklisted = NSManagedObject(entity: entity, insertInto: managedContext)
        
        blacklisted.setValue(id, forKeyPath: "id")
        blacklisted.setValue(name, forKeyPath: "name")
        blacklisted.setValue(phoneNumber.formatPhoneNumber, forKeyPath: "phoneNumber")

        do {
            try managedContext.save()
            
            var blacklist = loadBlocked()
            blacklist.append(blacklisted)
            
            print("blacklisted count: \(countBlocked())")  // <<-------------
            saveContext()
            
        } catch let error as NSError {
            print("Error: \(NSLocalizedString("ContactView.contactSection.coreData.saveError", comment: "")) \n\tInfo: \(error), \(error.userInfo)")
        }
        
        reloadExtension()
    }
    
    
    
    
    
    
    public func deleteFromBlacklist(_ phoneNumber: String) {
        
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Blacklisted")
        fetchRequest.predicate = NSPredicate(format: "phoneNumber == %lld", phoneNumber.formatPhoneNumber)

        do {
            let results = try managedContext.fetch(fetchRequest)
            for result in results {
                managedContext.delete(result as! NSManagedObject)
            }
            
            try managedContext.save()
            
            print("blacklisted count: \(countBlocked())")  // <<-------------
            saveContext()
            
        } catch {
            print("Error: " + NSLocalizedString("ContactView.contactSection.coreData.fetchError", comment: ""))
        }
        
        reloadExtension()
    }
    
}


