//
//  CallDirectoryHandler.swift
//  CallBlockerExtension
//
//  Created by tresrrr on 08/06/2021.
//

import Foundation
import CallKit
import CoreData
import CallBlockerData

class CallDirectoryHandler: CXCallDirectoryProvider {
    
    private let callerData = CallBlockerData()
    
    private func callers() throws -> [Blacklisted]  {
        let fetchRequest:NSFetchRequest<Blacklisted> = self.callerData.fetchRequest()
        let callers = try self.callerData.context.fetch(fetchRequest)
        
        NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > callers.count: \(callers.count)")

        return callers
    }
    
    override func beginRequest(with context: CXCallDirectoryExtensionContext) {
        context.delegate = self

        do {
            try addAllBlockingPhoneNumbers(to: context)
        } catch {
            NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > beginRequest > addAllBlockingPhoneNumbers  ERROR")
            let error = NSError(domain: "CallDirectoryHandler", code: 1, userInfo: nil)
            context.cancelRequest(withError: error)
            return
        }
        
//        do {
//            try addAllIdentificationPhoneNumbers(to: context)
//        } catch {
//            NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > beginRequest > addAllIdentificationPhoneNumbers  ERROR")
//            let error = NSError(domain: "CallDirectoryHandler", code: 2, userInfo: nil)
//            context.cancelRequest(withError: error)
//            return
//        }

        context.completeRequest()
    }
    
    private func addAllBlockingPhoneNumbers(to context: CXCallDirectoryExtensionContext) throws {
        
        //context.addBlockingEntry(withNextSequentialPhoneNumber: 678772660)
//        NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > addAllBlockingPhoneNumbers: caller.phoneNumber > \(678772660)")
        
        if let callers = try? self.callers() {
                        
            context.removeAllBlockingEntries()
            
            for caller in callers {
                context.addBlockingEntry(withNextSequentialPhoneNumber: caller.phoneNumber )
                NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > addAllBlockingPhoneNumbers: caller.phoneNumber > \(caller.phoneNumber)")
            }
        }
    }
    
//    private func addAllIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext) throws {
//
//        NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > addAllIdentificationPhoneNumbers")
//
//        if let callers = try? self.callers() {
//            for caller in callers {
//                if let name = caller.name {
//                    context.addIdentificationEntry(withNextSequentialPhoneNumber: caller.phoneNumber, label: name)
//                }
//            }
//        }
//    }
    
    
    
//    private func addAllBlockingPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
//
//        //NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > addAllBlockingPhoneNumbers")
//
//        if let callers = try? self.callers(blocked: true) {
//            for caller in callers {
//                context.addBlockingEntry(withNextSequentialPhoneNumber: caller.phoneNumber )
//
//                NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > addAllBlockingPhoneNumbers: caller.phoneNumber > \(caller.phoneNumber)")
//            }
//        }
//    }
//
//
//    private func addOrRemoveIncrementalBlockingPhoneNumbers(to context: CXCallDirectoryExtensionContext, since date: Date) {
//
//        NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > addOrRemoveIncrementalBlockingPhoneNumbers")
//
//        if let callers = try? self.callers(blocked: true, includeRemoved: true, since: date) {
//            for caller in callers {
////                if caller.isRemoved {
////                    context.removeBlockingEntry(withPhoneNumber: caller.phoneNumber)
////                } else {
////                    context.addBlockingEntry(withNextSequentialPhoneNumber: caller.phoneNumber)
////                }
//                context.addBlockingEntry(withNextSequentialPhoneNumber: caller.phoneNumber)
//            }
//        }
//    }
//
//    private func addAllIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
//
//        NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > addAllIdentificationPhoneNumbers")
//
//        if let callers = try? self.callers(blocked: false) {
//            for caller in callers {
//                if let name = caller.name {
//                    context.addIdentificationEntry(withNextSequentialPhoneNumber: caller.phoneNumber, label: name)
//                }
//            }
//        }
//    }
//
//    private func addOrRemoveIncrementalIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext, since date: Date) {
//
//        NSLog("\t>CallDirectoryHandler > CXCallDirectoryProvider > addOrRemoveIncrementalIdentificationPhoneNumbers")
//
//        if let callers = try? self.callers(blocked: false, includeRemoved: true, since: date) {
//            for caller in callers {
////                if caller.isRemoved {
////                    context.removeIdentificationEntry(withPhoneNumber: caller.phoneNumber)
////                } else {
////                    if let name = caller.name {
////                        context.addIdentificationEntry(withNextSequentialPhoneNumber: caller.phoneNumber, label: name)
////                    }
////                }
//                if let name = caller.name {
//                    context.addIdentificationEntry(withNextSequentialPhoneNumber: caller.phoneNumber, label: name)
//                }
//            }
//        }
//    }
    
}

extension CallDirectoryHandler: CXCallDirectoryExtensionContextDelegate {
    
    func requestFailed(for extensionContext: CXCallDirectoryExtensionContext, withError error: Error) {
        // An error occurred while adding blocking or identification entries, check the NSError for details.
        // For Call Directory error codes, see the CXErrorCodeCallDirectoryManagerError enum in <CallKit/CXError.h>.
        //
        // This may be used to store the error details in a location accessible by the extension's containing app, so that the
        // app may be notified about errors which occured while loading data even if the request to load data was initiated by
        // the user in Settings instead of via the app itself.
    }
    
}
